---
- name: Check if resource group already exists
  azure.azcollection.azure_rm_resourcegroup_info:
    name: "{{ azure_resource_group }}"
  register: resource_group

- name: Create resource group and tag it - {{ azure_resource_group }}
  azure.azcollection.azure_rm_resourcegroup:
    name: "{{ azure_resource_group }}"
    location: "{{ azure_location }}"
    state: present
    tags:
      ansible_ssa_deployed: "true"
  when: resource_group.resourcegroups == []

- name: Set private network
  ansible.builtin.set_fact:
    private_network: "{{ azure_private_network }}"
  when: azure_private_network is defined

- name: Set private network
  ansible.builtin.set_fact:
    private_network: "{{ instance_name }}"
  when: azure_private_network is not defined

- name: Create virtual network {{ private_network }}
  azure.azcollection.azure_rm_virtualnetwork:
    resource_group: "{{ azure_resource_group }}"
    name: "{{ private_network }}"
    address_prefixes_cidr: "10.1.0.0/16"
    state: present

- name: Create subnet {{ private_network }}
  azure.azcollection.azure_rm_subnet:
    resource_group: "{{ azure_resource_group }}"
    virtual_network_name: "{{ private_network }}"
    name: "{{ private_network }}"
    address_prefix_cidr: "10.1.0.0/24"

- name: Set port lists for Linux
  ansible.builtin.set_fact:
    port_list:
      - 80
      - 443
      - 22
  when: instance_type == "linux"

- name: Set port lists for Windows
  ansible.builtin.set_fact:
    port_list:
      - 80
      - 443
      - 3389
      - 5986
  when: instance_type == "windows"

- name: Expand port list with additional port
  ansible.builtin.set_fact:
    port_list: "{{ port_list + [instance_additional_port] }}"
  when: instance_additional_port is defined

- name: Expand port list with additional ports
  ansible.builtin.set_fact:
    port_list: "{{ port_list + instance_additional_ports }}"
  when: instance_additional_ports is defined

- name: Create Linux Virtual Machine - {{ instance_name }}
  azure.azcollection.azure_rm_virtualmachine:
    resource_group: "{{ azure_resource_group }}"
    name: "{{ instance_name }}"
    vm_size: "{{ instance_flavor }}"
    managed_disk_type: "{{ azure_disk_type }}"
    admin_username: ansible
    virtual_network_name: "{{ private_network }}"
    open_ports: "{{ port_list }}"
    ssh_password_enabled: false
    ssh_public_keys:
      - path: /home/ansible/.ssh/authorized_keys
        key_data: "{{ azure_ssh_public_key }}"
    image:
      offer: "{{ azure_image_offer }}"
      publisher: "{{ azure_image_publisher }}"
      sku: "{{ azure_image_sku }}"
      version: latest
    state: present
  register: linux_vm
  when: instance_type == "linux"
  retries: 10
  until: linux_vm.ansible_facts.azure_vm.powerstate is defined

- name: Create Windows Virtual Machine - {{ instance_name }}
  azure.azcollection.azure_rm_virtualmachine:
    resource_group: "{{ azure_resource_group }}"
    name: "{{ instance_name }}"
    vm_size: "{{ instance_flavor }}"
    managed_disk_type: "{{ azure_disk_type }}"
    admin_username: ansible
    admin_password: "{{ windows_admin_password }}"
    os_type: Windows
    virtual_network_name: "{{ private_network }}"
    open_ports: "{{ port_list }}"
    image:
      offer: "{{ azure_image_offer }}"
      publisher: "{{ azure_image_publisher }}"
      sku: "{{ azure_image_sku }}"
      version: latest
    state: present
  register: windows_vm
  when: instance_type == "windows"
  retries: 10
  until: windows_vm.ansible_facts.azure_vm.powerstate is defined

- name: Create VM script extension to enable HTTPS WinRM listener
  azure.azcollection.azure_rm_virtualmachineextension:
    name: winrm-extension
    resource_group: "{{ azure_resource_group }}"
    virtual_machine_name: "{{ instance_name }}"
    publisher: Microsoft.Compute
    virtual_machine_extension_type: CustomScriptExtension
    type_handler_version: '1.9'
    settings: '{"fileUris": ["https://raw.githubusercontent.com/ansible/ansible/v2.15.1/examples/scripts/ConfigureRemotingForAnsible.ps1"],"commandToExecute": "powershell -ExecutionPolicy Unrestricted -File ConfigureRemotingForAnsible.ps1"}'
    auto_upgrade_minor_version: true
  when: instance_type == "windows"

- name: Store Linux VM facts
  ansible.builtin.set_fact:
    vm: "{{ linux_vm.ansible_facts }}"
  when: instance_type == "linux"

- name: Store Windows VM facts
  ansible.builtin.set_fact:
    vm: "{{ windows_vm.ansible_facts }}"
  when: instance_type == "windows"

- name: Gather public IP address info
  azure.azcollection.azure_rm_publicipaddress_info:
    resource_group: "{{ azure_resource_group }}"
    name: "{{ vm.azure_vm.network_profile.network_interfaces[0].properties.ip_configurations[0].public_ip_address.name }}"
  register: _azure_public_ip

- name: Print instance details
  ansible.builtin.debug:
    msg: "Instance IP address is {{ _azure_public_ip.publicipaddresses[0].ip_address }}"

- name: Update DNS record
  ansible.builtin.include_role:
    name: nsupdate
  vars:
    ipaddress: "{{ _azure_public_ip.publicipaddresses[0].ip_address }}"
    shortname: "{{ instance_name }}"
    mode: update
  when: dns_update | bool

- name: Add DNS hostname to in memory inventory, if DNS defined
  ansible.builtin.add_host:
    name: "{{ instance_name }}"
    ansible_host: "{{ instance_name }}.{{ dns_suffix }}.ansible-labs.de"
    ansible_user: ansible
    groups:
      - "{{ instance_group | default('all_hosts') }}"
  when: dns_update | bool

- name: Add public IP to in memory inventory, if DNS is not defined
  ansible.builtin.add_host:
    name: "{{ instance_name }}"
    ansible_host: "{{ _azure_public_ip.publicipaddresses[0].ip_address }}"
    ansible_user: ansible
    groups:
      - "{{ instance_group | default('all_hosts') }}"
  when: dns_update is false

- name: Wait for instance port 22
  ansible.builtin.wait_for:
    host: "{{ _azure_public_ip.publicipaddresses[0].ip_address }}"
    timeout: 300
    delay: 10
    port: 22
  when:
    - instance_type == "linux"
    - instance_wait_for_connection | bool

- name: Wait for instance port 5986
  ansible.builtin.wait_for:
    host: "{{ _azure_public_ip.publicipaddresses[0].ip_address }}"
    timeout: 300
    delay: 10
    port: 5986
  when:
    - instance_type == "windows"
    - instance_wait_for_connection | bool
