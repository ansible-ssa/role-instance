# Ansible Role: instance

This role will help to create and remove instances on several different providers. It can also be used to demonstrate multi cloud use cases.

**WARNING: Support for Red Hat Virtualization currently has no owner and will no longer be maintained and eventually be removed! If you're interested in keeping it alive, please volunteer as a maintainer.**

## How to install this role

The Ansible SSA collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

The role is optimized to be used from the [Playbook RHAAP](https://gitlab.com/ansible-ssa/playbook-rhaap) which will be called by a Job Template created by this role.

### GCP

If you want to use it in your playbook for GCP:

```yaml
- name: deploy instance
  hosts: localhost
  connection: local
  vars:
    type: gcp
    instance_name: demo
    instance_flavor: n1-standard-1
    instance_disk_size: 20
  include_role:
    name: ansible_ssa.general.instance
```

### AWS EC2

If you want to use it in your playbook for AWS EC2:

```yaml
---
- name: deploy instance
  hosts: localhost
  connection: local
  vars:
    type: ec2
    instance_name: grafana_server
    instance_flavor: t2.large
    instance_disk_size: 10
    instance_additional_port: "3000"
    ec2_image_name: "RHEL-9.3.0_HVM-*x86_64"
    ec2_key_pair: "ssh-rsa ABCDEFGHIJKLMNOPQRSTUVWYZÅÄÖ"
  include_role:
    name: ansible_ssa.general.instance
```

### VMware

If you want to use it in your playbook for VMWARE:

```yaml
- name: deploy instance
  hosts: localhost
  connection: local
  vars:
    type: vmware
    instance_name: demo
    instance_group: controller
    instance_parameters: "{{ controller_instance_parameters | default ({}) }}"
  include_role:
    name: ansible_ssa.general.instance
```

or for vmware (second example):

```yaml
- name: deploy instance
  hosts: localhost
  connection: local
  vars:
    type: vmware
    instance_name: demo
    instance_group: controller
    instance_parameters.vmware_instance_diskgb: '10'
    instance_parameters.vmware_instance_memmb: '8192'
    instance_parameters.vmware_instance_cpus: '2
  include_role:
    name: ansible_ssa.general.instance
```

### Creating multiple VMs

To create multiple VMs, there are some things to keep in mind. First off simply loop the creation of virtual machines, as such:

```yaml
- name: Create {{ max_number }} of VMs
  ansible.builtin.include_role:
    name: ansible_ssa.general.instance
  vars:
    instance_name: "myvm-{{item}}"
  with_sequence:
    - "1-{{ max_number }}"
```

To prevent hitting your quotas on number of networks or VPCs created, when creating VMs on Azure, GCP or AWS, ensure to define the corresponding network/VPC variable, if you do not do that, a unique network/VPC will be created for each VM.

* For AWS, set:

```yaml
ec2_vpc_name: my-vpc-name
```

* For GCP, set:

```yaml
gcp_network_name: my-network-name
```

* For Azure, set:

```yaml
do_vpc_name: my-vpc-name
```

### Remove VM

To remove or delete a VM or instance, use the same role, but with the "remove" variables set to true.

```yaml
- name: Remove instance
  hosts: localhost
  connection: local
  vars:
    type: gcp
    instance_name: demo
    remove: true
  include_role:
    name: ansible_ssa.general.instance
```

Instead of using the role directly, consider using the [Ansible SSA Collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection)

## Inventory

The role will automatically add created systems if you have set ```dyndns: true```. If you then also define ```instance_group: something``` the role will add the system to that specific group. If not defined, it simply adds it to the inventory with no group.

If you are not using ```dyndns: true```, then automate against systems which you have created, easiest is to use a dynamic inventory plugin. An easy way to find systems you have created, is to use filters, which allows you to find systems which has specific tags.
This role will create a Name:SystemNameYouGave tag automatically.

Read more here:

* EC2 inventory plugin: [https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html)
* GCP inventory plugin: [https://docs.ansible.com/ansible/latest/collections/google/cloud/gcp_compute_inventory.html](https://docs.ansible.com/ansible/latest/collections/google/cloud/gcp_compute_inventory.html)
* Azure inventory plugin: [https://docs.ansible.com/ansible/latest/collections/azure/azcollection/azure_rm_inventory.html](https://docs.ansible.com/ansible/latest/collections/azure/azcollection/azure_rm_inventory.html)
* VMware inventory plugin: [https://docs.ansible.com/ansible/latest/collections/community/vmware/vmware_vm_inventory_inventory.html](https://docs.ansible.com/ansible/latest/collections/community/vmware/vmware_vm_inventory_inventory.html)

You can also use this trick, where you access the previously created linux_vm or windows_vm variables, which are registered when such a system is created. As such:

```yaml
---
- name: deploy instance
  hosts: localhost
  connection: local
  vars:
    type: ec2
    instance_name: grafana_server
    instance_flavor: t2.large
    instance_disk_size: 10
    instance_additional_port: "3000"
    ec2_image_name: "RHEL-9.3.0_HVM-*x86_64*"
    ec2_key_pair: "ssh-rsa ABCDEFGHIJKLMNOPQRSTUVWYZÅÄÖ"
  include_role:
    name: ansible_ssa.general.instance

- name: do things
  hosts: all
  vars:
    ec2_instance_feedback: "{{ hostvars['localhost']['linux_vm'] }}"
  tasks:
    - name: Add created host to in-memory inventory
      add_host: hostname={{ item.public_ip_address }} groups=linux
      with_items: "{{ ec2_instance_feedback.instances }}"

    - name: Ping systems
      ansible.builtin.ping:
        data: pong from Ansible
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
